FROM golang:1.20.3-alpine as Builder

ENV GOARCH=amd64
ENV GOOS=linux

WORKDIR /usr/src/app
COPY ./ ./
RUN go build -o bin/

FROM golang:1.20.3-alpine as runner

WORKDIR /usr/src/app/
COPY --from=Builder /usr/src/app/bin/sweden-electricity-price /usr/src/app/sweden-electricity-price
ENTRYPOINT [ "/usr/src/app/sweden-electricity-price" ]
