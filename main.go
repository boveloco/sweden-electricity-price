package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
)

// Had to "overload" the function to actually get what I wanted...
func getEnv(params ...string) string {
	value := os.Getenv(params[0])
	if len(value) == 0 {
		if len(params[1]) == 0 {
			panic("Could not find variable: " + params[0])
		}
		return params[1]
	}
	return value
}

var (
	// iNIT iNFLUX INFOS
	INFLUX_HOST        = getEnv("INFLUX_HOST", "influxdb")
	INFLUX_TOKEN       = getEnv("INFLUX_TOKEN")
	INFLUX_PORT        = getEnv("INFLUX_PORT", "8086")
	INFLUX_BUCKET_NAME = getEnv("INFLUX_BUCKET_NAME", "swe-ele-test")
	INFLUX_ORG_NAME    = getEnv("INFLUX_ORG_NAME", "null")

	// INIT ELECTRICITY API INFOS
	API_URI = getEnv("API_URI", "https://mgrey.se/espot?format=json")
)

func main() {

	// Create a new client using an InfluxDB server base URL and an authentication token
	c := influxdb2.NewClientWithOptions("http://"+INFLUX_HOST+":"+INFLUX_PORT, INFLUX_TOKEN,
		influxdb2.DefaultOptions().SetBatchSize(20))
	writeAPI := c.WriteAPI(INFLUX_ORG_NAME, INFLUX_BUCKET_NAME)
	check, err := c.Health(context.Background())

	if (err != nil) && check.Status != "pass" {
		panic("Error Connecting to Database!!")
	}

	errorsCh := writeAPI.Errors()
	// Create go proc for reading and logging errors
	go func() {
		for err := range errorsCh {
			fmt.Printf("write error: %s\n", err.Error())
		}
	}()

	fmt.Printf("DEBUG>> Adding price for %s", time.Now().String())

	d := getElectricityCost(API_URI)

	// Create point using fluent style
	p := influxdb2.NewPointWithMeasurement("electricity_price").
		AddTag("region", "se1").
		AddField("price_sek", d.Se1[0].Price_sek).
		AddField("price_eur", d.Se1[0].Price_eur).
		AddField("kmeans", d.Se1[0].Kmeans).
		SetTime(time.Now())
	q := influxdb2.NewPointWithMeasurement("electricity_price").
		AddTag("region", "se2").
		AddField("price_sek", d.Se2[0].Price_sek).
		AddField("price_eur", d.Se2[0].Price_eur).
		AddField("kmeans", d.Se2[0].Kmeans).
		SetTime(time.Now())

	r := influxdb2.NewPointWithMeasurement("electricity_price").
		AddTag("region", "se3").
		AddField("price_sek", d.Se3[0].Price_sek).
		AddField("price_eur", d.Se3[0].Price_eur).
		AddField("kmeans", d.Se3[0].Kmeans).
		SetTime(time.Now())
	s := influxdb2.NewPointWithMeasurement("electricity_price").
		AddTag("region", "se4").
		AddField("price_sek", d.Se4[0].Price_sek).
		AddField("price_eur", d.Se4[0].Price_eur).
		AddField("kmeans", d.Se4[0].Kmeans).
		SetTime(time.Now())

	writeAPI.WritePoint(p)
	writeAPI.WritePoint(q)
	writeAPI.WritePoint(r)
	writeAPI.WritePoint(s)

	writeAPI.Flush()

	// Ensures background processes finishes
	c.Close()
}

func getElectricityCost(uri string) Data {
	spaceClient := http.Client{
		Timeout: time.Second * 5, // Timeout after 2 seconds
	}

	req, err := http.NewRequest(http.MethodGet, uri, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "gitlab.com/boveloco/electricity-cost-sweden")

	res, getErr := spaceClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}

	if res.Body != nil {
		defer res.Body.Close()
	}

	var d Data

	body, readErr := io.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	jsonErr := json.Unmarshal(body, &d)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}
	return d
}
